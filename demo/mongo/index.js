module.exports = app => {
	//用户列表
	app.get('/list', async (r, h) => {
		console.log(app.model);
		let userList = await app.model.User.list();
		h.json({userList});
	});
	//增加用户
	app.get('/add', async (r, h) => {
		let newUser = await app.model.User.save({
			user_name: '用户名',
			nick_name: '昵称'
		});
		let userList = await app.model.User.list();
		h.json({userList});
	});
}