module.exports = app => {
	//加载模块：自定义路由
	app.use('/', './home');
	app.use('/mismis', './mis');
	//加载模块：直接加载
	app.use('/config');
	//加载模块：目录加载
	app.use('/anyx', './any/*');
	app.use('/transgod', './server/*');
	//直接写路由
	app.get('/get', (r, h) => {
		h.text('get方法');
	});

	//测试：跨模块调用
	app.use('/mixin');
	//测试：模板渲染
	app.use('/tpl', './tpl');
	//测试：读取配置
	app.get('/conf', (r, h) => {
		h.json(app.$['atweb-demo'].config);
	});
	//测试：mongo数据库使用
	app.use('/mongo', './mongo');

	//无路由插件引用
	//app.use('/$', './插件目录');

	//单元测试
	app.use('/test', './test');

}