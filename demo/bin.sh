#!/bin/bash
if [ "$1" = 'docker' ]; then 
	cd /atweb
	#更新npm依赖模块
	rm package.json
	rm package-lock.json
	cp /atweb/app/package.json /atweb/package.json
	npm install --registry=https://registry.npm.taobao.org
	cd /atweb
	cp /atweb/node_modules/atweb/bin/web.js /atweb/web.js
	if [ "$2" = 'dev' ]; then
		#启动服务
		pm2 start web.js --name='app' --watch --ignore-watch='storage node_modules yml app/.git app/_data app/web app/package.json' -- /atweb/app
		cd /atweb/app/web
		fis3 release dev -wd /atweb/storage/assets
		pm2 logs app
	fi
	if [ "$2" = 'online' ]; then
		#启动服务
		pm2 start web.js --name='app' -- /atweb/app
		cd /atweb/app/web
		fis3 release online -wd /atweb/storage/assets
		pm2 logs app
	fi
else
	#解析json
	function json_extract(){
	  local key=$1
	  local json=$2

	  local string_regex='"([^"\]|\\.)*"'
	  local number_regex='-?(0|[1-9][0-9]*)(\.[0-9]+)?([eE][+-]?[0-9]+)?'
	  local value_regex="${string_regex}|${number_regex}|true|false|null"
	  local pair_regex="\"${key}\"[[:space:]]*:[[:space:]]*(${value_regex})"

	  if [[ ${json} =~ ${pair_regex} ]]; then
	    echo $(sed 's/^"\|"$//g' <<< "${BASH_REMATCH[1]}")
	  else
	    return 1
	  fi
	}

	#基础配置文件
	config=`cat $PWD/config.json`
	#应用名称
	name=`json_extract name "$config"`
	APP=${name//\"/}
	#应用端口
	port=`json_extract dockerPort "$config"`
	PORT=${port//\"/}
	#存储路径
	DATAPATH=`pwd`/_data

	#本地化配置
	localConfig=`cat $PWD/.config.json`
	if [ -n "$localConfig" ]; then 
		if [ -n "$localConfig" ]; then 
			name=`json_extract name "$localConfig"`
			APP=${name//\"/}
			port=`json_extract dockerPort "$localConfig"`
			PORT=$port
			datapath=`json_extract datapath "$localConfig"`
			DATAPATH=$datapath
		fi
	fi

#读取yml文件
yml=`cat << EOF
version: '2'
services:
  APP:
    image: registry.docker-cn.com/nicozhang/atweb:v0.03
    container_name: APP
    restart: always
    network_mode: NET
    volumes:
      - "/home/gitlab-runner/builds/5be70764/0/stefren/ci-test/demo:/atweb/app:rw"
      - "DATAPATH:/data:rw"
      - "DATAPATH/node_modules:/atweb/node_modules:rw"
      - "/home/gitlab-runner/builds/5be70764/0/stefren/ci-test/demo/../:/atweb/node_modules/atweb:rw"
      - "DATAPATH/logs/pm2:/root/.pm2/logs:rw"
      - "DATAPATH/logs/log4js:/root/.log4js/logs:rw"
      - "/var/run/docker.sock:/var/run/docker.sock:rw"
      - "DATAPATH/yml:/atweb/yml"
    command: bash -c "chmod +x -R /atweb/app/bin.sh;/atweb/app/bin.sh docker DEV"
    ports:
      - "PORT:80"
      - "9888:9888"
    environment:
      network: NET
      datapath: DATAPATH
    tty: true
EOF
`
	#替换应用名称
	yml=${yml//APP/$APP}
	#替换端口映射
	yml=${yml//PORT/$PORT}
	#替换数据存储路径
	yml=${yml//DATAPATH/$DATAPATH}

	#创建私有网络
	docker network create net_$APP
	yml=${yml//NET/net_$APP}
	case "$1" in
		#更新代码
		"update")
			echo '更新中'
			git pull
			docker exec -it $APP /bin/bash /atweb/app/bin.sh docker online
			echo '更新成功'
		;;
		#重启app
		"restart")
			docker exec -it $APP pm2 restart app
		;;
		#停止
		"stop")
			docker-compose -f $DATAPATH/.docker-compose.yml stop
		;;	
		#进入容器
		"bash")
			docker exec -it $APP /bin/bash
		;;
		#上线
		"online")
			yml=${yml//DEV/online}
			echo "${yml}" > $DATAPATH/.docker-compose.yml
			#启动应用
			docker-compose -f $DATAPATH/.docker-compose.yml up -d
		;;
		#查看实时日志
		"log")
			docker exec -it $APP pm2 logs
		;;
		#查看docker日志	
		"logs")
			docker logs -f $APP
		;;
		#更新npm
		"npm")
			#启动nginx
			docker exec -it $APP /bin/bash /atweb/app/bin.sh docker npm
		;;	
		#本地开发
		*)
			yml=${yml//DEV/dev}
			if [ ! -d "$DATAPATH" ]; then
				mkdir $DATAPATH
			fi
			echo "${yml}" > $DATAPATH/.docker-compose.yml
			#启动应用
			docker-compose -f $DATAPATH/.docker-compose.yml up -d
			docker logs -f $APP
		;;
	esac
fi