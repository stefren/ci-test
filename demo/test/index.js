const unitTest = require('atman-unit-test');
module.exports = app => {
    app.get('/', async function (r, h) {
        var modules = [];
        var cases = [];
        global.ATWEB.rmap.forEach(function (route) {
            let module_name = "模块单元测试："+route.modulePath;
            if (modules.indexOf(module_name) == -1) {
                modules.push(module_name)
            }
            let _case = {
                body: {
                    case_name: route.path,
                    method: route.method,
                    auth: true,
                    target_url: "http://localhost"+route.path,
                    expects: {
                    }
                },
                _t: route.modulePath
            }
            cases.push(_case);
        })
        var json_arr = [];
        modules.forEach(function (module) {
            var json = {
                title: module,
                cases: []
            }
            cases.forEach(function (_case) {
                if ("模块单元测试："+_case._t === module) {
                    json.cases.push(_case.body);
                }
            })
            json_arr.push(json)
        })
        var test_file = await unitTest.makeTestFile(json_arr, '/atweb/app/_data/test/');
        var report_name = 'all_test';
        h.jump('/test/waiting?report='+report_name+'&test='+test_file);
    });

    app.get('/waiting', function (r, h) {
        console.log('osss');
        app.io.sockets.on('connection', function(socket){
            socket.on('run_test', function(data){
                var report_name = data.report_name;
                var test_file = data.test_file;
                unitTest.runTest(test_file, report_name, '/atweb/app/_data/test', function(call){
                    if (call === 'finish') {
                        socket.emit('finished', {test: report_name+'.html'})
                    }
                })
            })
        })
        h.tpl('waiting');
    });

    app.get('/api', function (r, h) {
        // setTimeout(function () {
        //     h.json({foo: 'bar'})
        // }, 1000);
        h.json({foo: 'bar'});
    });

    //socket.io
    // app.io.sockets.on('connection', function (socket) {
    //     socket.on('run_test', function (data) {
    //         var report_name = data.report_name;
    //         var test_file = data.test_file;
    //         console.log(test_file, report_name);
    //         unitTest.runTest(test_file, report_name, '/atweb/app/_data/test', function (call) {
    //             if (call === 'finish') {
    //                 socket.emit('finished', {test: report_name+'.html'});
    //             }
    //         })
    //     })
    // })
}