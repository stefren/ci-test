<html>
<head>
    <META HTTP-EQUIV="pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache, must-revalidate">
    <META HTTP-EQUIV="expires" CONTENT="0">
</head>
<body>
<div id="content">
    <p>正在生成测试报告，请等待....</p>
</div>
</body>
<script src="http://192.168.1.11:9888/socket.io/socket.io.js"></script>
<script>
    var query = window.location.search;
    var params = [];
    query.split('&').forEach(function (item) {
        params.push(item.split('=')[1]);
    })
    console.log(params);
    var socket = io.connect('http://192.168.1.11:9888');
    console.log(socket);
    socket.emit('run_test', {report_name: params[0], test_file: params[1]});
    socket.on('finished', function (data) {
        console.log('data is:',data);
        var url = data.test;
        window.location.href = '/report/'+url;
    })
</script>
</html>
