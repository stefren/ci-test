const fs = require('fs');
//挂载文档
module.exports = app => {
	//按模块搜索
	for(let key in global.ATWEB.mmap){
		let mitem = global.ATWEB.mmap[key];
		let dpath = `${mitem.modulePath}/docs/`;
		let docs = {};
		if(fs.existsSync(dpath)){
			let files = fs.readdirSync(dpath);
			if(files.length > 0){
				files.forEach(function(file){
					let _file = file.split(".");
					_file.reverse();
					let doc = (dpath + file);
					if(_file[0] == 'js'){
						let result = {},_result = {};
						//转换格式
						if(doc && fs.existsSync(doc)){
							result = require(doc);
							_result.description = result.name;
							_result.tags = result.tag || [];
							if(result.note){
								_result.notes = result.note;
							}
							_result.validate = {};
							if(result.post){
								_result.validate.payload = result.post;
							}		
							if(result.get){
								_result.validate.query = result.get;
							}		
							if(result.path){
								_result.validate.params = result.path;
							}
							docs[file.split(".")[0]] = _result;
						}else{
							docs[file.split(".")[0]] = {};
						}
					}
				});
			}
		}
		mitem.app.docs = docs || {};
	}
	//按路由挂载
	global.ATWEB.rmap.forEach((ritem,index) => {
		// 挂载doc
		let docName = ritem.path.split('/');
		docName.reverse();
		docName = docName[0];
		if(ritem.module.docs){
			let docInfo = ritem.module.docs[docName];
	       	for(_key in docInfo){
	       		if(_key == "tags"){
	       			docInfo.tags.forEach(titem => {
	       				global.ATWEB.rmap[index].config.tags.push(titem);
	       			});
	       		}else{
	        		global.ATWEB.rmap[index].config[_key] = docInfo[_key];
	       		}
	        }
        }
	});
}