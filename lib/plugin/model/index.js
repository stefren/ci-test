const fs = require('fs');
const mongoose = require('./mongoose');
module.exports = app => {
	for(let key in global.ATWEB.mmap){
		let mitem = global.ATWEB.mmap[key];
		let mpath = `${mitem.modulePath}/model/`;
		let model = {};
		let config = mitem.config;
		//初始化mongoose
		let dbhost = (config && config.database && config.database.mongoose)?config.database.mongoose.host:'';
		let dbname = (config && config.database && config.database.mongoose)?config.database.mongoose.dbname:'';
		mongoose.init(mitem.namespace, dbhost, dbname);
		if(fs.existsSync(mpath)){
			let files = fs.readdirSync(mpath);
			if(files.length > 0){
				files.forEach(function(file){
					let _file = file.split(".");
					_file.reverse();
					let _map = require(mpath + file);
					if(_file[0] == 'js'){
						model[file.split(".")[0]] = mongoose.exmodel(mitem.namespace, file.split(".")[0], _map);
					}else{
						//json配置文件
						model[file.split(".")[0]] = _map;
					}
				});
			}
		}
		mitem.app.model = model || {};
	}
}