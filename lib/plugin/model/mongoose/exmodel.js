var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const BaseModel = require("./base");

module.exports = (appname, modelName,srcModel) => {
	var model = {};
	//融合
	srcModel(model);
	if(model && global.ATWEB.db && global.ATWEB.db[appname]){
		//新建schema
		const _schema = new Schema(model.fieldMap);
		var _model = global.ATWEB.db[appname].model(modelName, _schema);
		//继承base，启用model实例
		var Model = new BaseModel(_model);
		Model.fieldMap = model.fieldMap;
		model.db = _model;
		for(let key in model){
			if(key != "fieldMap"){
				Model[key] = model[key];
			}
		}
		return Model;
	}
};