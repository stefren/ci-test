const mongoose = require('mongoose');

module.exports = {
	init: (appname, host, dbname) => {
		// console.log(`appname${appname}`);
		// console.log(`host${host}`);
		global.ATWEB.db = global.ATWEB.db || {};
		//初始化mogoose
		var mongoHost = host || 'mongo';
		if(dbname && !global.ATWEB.db[appname]){
			mongoose.Promise = global.Promise;
			let count = 0;
			let success = false;
			let con = () => {
				global.ATWEB.db[appname] = mongoose.connect(`mongodb://${mongoHost}/${dbname}`).connection;
				global.ATWEB.db[appname].on('error', (err) => {
					count++;
					console.log(`重试 第${count}次`);
					//setTimeout(con, 1500);
				});
				global.ATWEB.db[appname].once('open',function(){
					success = true;
					console.log(`${appname} mongoose连接成功`);
				});
			};
			setInterval(function(){
				if(!success){
					con();
				}
			}, 1500);
		}
	},
	exmodel: require('./exmodel')
};