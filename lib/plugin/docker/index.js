const fs = require('fs');
module.exports = async app => {
	// (async () => {
		for(let key in global.ATWEB.mmap){
			let mitem = global.ATWEB.mmap[key];
			let modulePath = mitem.modulePath;
			let config = mitem.config;
			let pk = mitem.pk;
			if(fs.existsSync(`${modulePath}/docker-compose.yml`)){
				if(fs.existsSync(`${modulePath}/package.json`)){
					pk = require(`${modulePath}/package.json`);
				}
				config = config || pk || {};
				let APP = config.name;
				let PORT = config.dockerPort || '';
				let DATAPATH = config.datapath || process.env.datapath || '$PWD/_data';
				let yml = fs.readFileSync(`${modulePath}/docker-compose.yml`, 'utf8');
				yml = yml.replace(/APP/g, APP);
				yml = yml.replace(/PORT/g, PORT);
				yml = yml.replace(/DATAPATH/g, DATAPATH);
				yml = yml.replace(/NET/g, process.env.network);
				fs.writeFileSync(`/atweb/yml/${APP}.docker-compose.yml`, yml, 'utf8');
				let cmdUp = `docker-compose -f /atweb/yml/${APP}.docker-compose.yml up -d`;
				//检测是否需要为docker镜像建立别名
				if(pk && pk.atweb && pk.atweb.alias){
					for(let key in pk.atweb.alias){
						fs.writeFileSync(`/usr/bin/${key}`, pk.atweb.alias[key], 'utf8');
						fs.chmodSync(`/usr/bin/${key}`, '777');
					}
				}
				await (function(){ 
					return new Promise((res ,rej) => {
						require('child_process').exec(cmdUp,{},function(err, result){							
							res(err);
						});
					});
				})();
			}
		}
	// })();
}