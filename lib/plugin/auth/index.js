module.exports = app => {
	//处理模块间验证依赖
	for(let key in global.ATWEB.mmap){
		let mitem = global.ATWEB.mmap[key];
		if(mitem.app.option && mitem.app.option.parent && mitem.app.option.parent.auth.authBasic){
			global.ATWEB.mmap[key].app.auth.authBasic = true;
			global.ATWEB.mmap[key].app.auth.authName = mitem.app.option.parent.auth.authName;
		}
	}
	//按路由挂载
	global.ATWEB.rmap.forEach((ritem,index) => {
		if(ritem.module.auth.authBasic){
			let authBasic = true;
			ritem.module.auth._unless.forEach(uitem => {
				if(uitem == ritem.path){
					authBasic = false;
				}
			});
			// ritem.module.auth._less.forEach(uitem => {
			// 	if(uitem == _url){
			// 		authBasic = true;
			// 	}else{
			// 		authBasic = true;
			// 	}
			// });
			//basic登录
			if(authBasic){
				global.ATWEB.rmap[index].config.auth = ritem.module.auth.authName;
			}
		}
	});
}