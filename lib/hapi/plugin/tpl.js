var	nunjucks = require('nunjucks');
const Path = require('path');

// nunjucks.configure({
//     autoescape: true,
//     watch: true,
//     noCache: true
// });

const tpl = {
    name: 'tpl',
    version: '1.0.0',
    register: function (server, options) {
		server.decorate('toolkit', 'renders', function(tpl,data,request){
			var tail = tpl.substring(tpl.length-4,tpl.length);
			tpl = tail==".tpl"?tpl:tpl+".tpl";
		    data = data || {};
		    data._data = JSON.stringify(data);
		    let tplInfo = Path.parse(tpl);
		    nunjucks.configure(tplInfo.dir, { autoescape: true });
		    var output = nunjucks.render(tplInfo.base, data);
		    return (output);
		});
    }
};

module.exports = tpl;