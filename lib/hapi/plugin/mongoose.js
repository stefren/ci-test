var mongoose = require('mongoose');

const _mongoose = {
    name: 'mongoose',
    version: '1.0.0',
    register: function (server, options) {
    	if(options && options.config && options.config.host){
		    //初始化mogoose
		    var mongoHost = options.config.host || 'mongo',
		    	dbname = options.config.dbname;
			mongoose.Promise = global.Promise;
			db = mongoose.connect(`mongodb://${mongoHost}/${dbname}`).connection;
			db.on('error',console.error.bind(console,'mongoose连接错误:'));
			db.once('open',function(){
				console.log('mongoose连接成功');
			});
			server.mongoose = mongoose;
    	}
    }
};

module.exports = _mongoose;