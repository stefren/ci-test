var client = require('redis');

const redis = {
    name: 'redis',
    version: '1.0.0',
    register: function (server, options) {
    	if(options && options.config){
    		var port = options.config.port || '6379',
    			host = options.config.host || 'redis';
			var redisClient  = client.createClient(port, host);
			console.log('redis连接成功');
			var redis = {
				"get": function(key){
					return new Promise(function(resolve, reject){
						redisClient.get(key,function(err,data){
							err && reject(err);
							resolve(data);
						});
					})
				},	
				"set": function(key,value){
					redisClient.set(key,value);
				},
				"client": redisClient
			};
			server.redis = redis;
    	}
    }
};

module.exports = redis;