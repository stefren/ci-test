module.exports = (config,path) => {
	return [
		{
		    plugin: require('hapi-auth-basic'),
		    options: {}
		},	
		{
			plugin: require('./hapi-server-session'),
			options: {
				name: 'sid',
				cookie: {
					path: '/',
					isSecure: false
				}
			}
		},	
		{
		    plugin: require('vision'),
		    options: {}
		},
		// //静态资源插件
		{
		    plugin: require('inert'),
		    options: {}
		},		
		{
		    plugin: require('h2o2'),
		    options: {}
		},			
		//静态资源插件
		{
		    plugin: require('./static'),
		    options: {
		    	path: path,
		    	config: config['static'] || null
		    }
		},
		//API文档插件
		{
		    plugin: require('hapi-swagger'),
		    options: {
				info: {
	                title: '文档',
	                version: 'v9.1.1',
	            },
	            host: `localhost:${config.dockerPort}`,
				consumes: ['application/json'],
				payloadType: 'form',
				lang: 'zh-cn'
		    }
		},
		//模板插件
		{
		    plugin: require('./tpl'),
		    options: {}
		},	
		//错误代码		
		{
			plugin: require('./errer-code'),
			options: {
		    	config: config.errerCode || null
			}
		},
		//redis
		{
		    plugin: require("./redis"),
		    options: {
		    	config: config.datebase?(config.datebase.redis?config.datebase.redis:null):null
		    }
		},			
		//mongoose
		{
		    plugin: require("./mongoose"),
		    options: {
		    	config: config.datebase?(config.datebase.mongoose?config.datebase.mongoose:null):null
		    }
		}
	]
}