const Path = require('path');
const fs = require('fs');
//底层链
global.ATWEB = {
	//模块链
	mmap: {},
	//路由链
	rmap: [],
	//数据库链
	db: {}
};
function atweb(option){
	var me = this;
	me.option = option;
	//引入模块
	me.use = (url, module) => {
		module = module || `./${url}`;
		//解析模块路径
		let modulePath;
		if(module.charAt(0,1) == '.'){
			modulePath = option.dirname + module.replace('.', '');
		}else{
			modulePath = module;
		}
		modulePath = modulePath.replace(/\/\//g, '/');
		//加载目录
		if(modulePath.indexOf('*') > -1){
			let dirs = modulePath.replace('*', "");
			if(fs.existsSync(dirs)){
				let files = fs.readdirSync(dirs);
				files.forEach(function(ele,index){
					var t = `${dirs}${ele}`;
					//过滤隐藏文件
					if(!(ele.charAt(0,1) == '.')){
					    var info = fs.statSync(t);
					    let furl;
					    if(info.isDirectory()){
					    	furl = option.url?option.url + url + '/' + ele:'';
					    }else{
					    	furl = option.url?option.url + url + '/' + (ele.split('.')[0]):'';
					    }
						let _module = require(t);
						let childOption = {
							name: '',
							url: furl,
							parent: me,
							dirname: t,
							_package: null,
							config: null
						};
						_use(furl, _module, childOption);
				    }     
				});
			}
		}else{
			//加载子模块
			let config = {};
			let _package = {};
			let name = '';
			url = option.url?option.url + url : url;
			//加载发布包
			if(fs.existsSync(`${modulePath}/package.json`)){
				_package = require(`${modulePath}/package.json`);
				if(fs.existsSync(`${modulePath}/config.json`)){
					config = require(`${modulePath}/config.json`);
				}
				module = require(`${modulePath}/index.js`);
			}else{
				module = require(modulePath);
			}
			url = url.replace(/\/\//g, '/');
			url = url.replace(/\/\//g, '/');
			let childOption = {
				namespace: _package.name || option.namespace,
				name: _package.name || '',
				url: url,
				parent: me,
				dirname: modulePath,
				_package,
				config
			};
			_use(url, module, childOption);
		}
		//加载模块
		function _use(url, module, childOption){
			//console.log(childOption);
			url = url.replace(/\/\//g, '/');
			url = url.replace(/\/\//g, '/');
			let children = new atweb(childOption);
			let mname = childOption._package && childOption._package.name?childOption._package.name:url;
			//追加模块链
			global.ATWEB.mmap[mname] = {
				namespace: childOption.namespace,
				name: mname,
				modulePath: childOption.dirname,
				pk: childOption._package,
				url: url,
				app: children,
				config: childOption.config
			};
			//反向执行模块代码
			module(children);
		};
	};
	//插入新方法
	me.addMethod = (name, fn) => {
		atweb.prototype[name] = fn;
	};
	//运行时插件，暂支持request
	me.on = function(event, fn){
		if(event == 'request'){
			global.HAPI.ext('onRequest', function(request, h){
				fn(request,h);
				return h.continue;
			});
		}
	};
	//模块验证
	me.auth = {
		basic: function(userList){
    		me.auth.authBasic = true;
			let authName = `auth-${Date.now()}`;
			let validate = function(request, username, password){
				let result = { credentials: {name: null}, isValid: false };
				userList.forEach(uitem => {
					uitem = uitem.split(':');
				    if (username == uitem[0] && password == uitem[1]){
				        result = { credentials: {name:uitem[0]}, isValid: true };
				    }
				});
				return result;
			};
    		global.HAPI.auth.strategy(me.auth.authName, 'basic', { validate });
    		return me.auth;
		},
		login: function(validate, option){
    		me.auth.authLogin = true;
			me._login = {
				fn: validate,
				option: option
			};
		},
		unless: function(list){
			me.auth._unless = list;
		},		
		less: function(list){
			me.auth._less = list;
		},
		_unless: [],
		_less: [],
		authName: `auth-${Date.now()}`,
		authBasic: false,
		authLogin: false
	};
	//路由方法
	["get","post","put","update","delete","*"].forEach(function(method){
		me[method] = (_url, fn, config={}) => {
			url = option.url + _url;
			url = url.replace(/\/\//g, '/');
			url = url.replace(/\/\//g, '/');
			url = (url == '/' || url.charAt(url.length-1)!='/')?url:url.substring(0, url.length-1);
			//记录路由信息
			global.ATWEB.rmap.push({
				path: url,
				method: method,
				handler: fn,
				config: config,
				modulePath: option.dirname,
				module: me,
				CONF: option.parent.option.config
			});
		};
	});
};

module.exports = atweb;