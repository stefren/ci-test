const Hapi = require('hapi');
const Path = require('path');
const atweb = require('./lib/atweb');
const server = Hapi.server({ port: 80 });
const io = require('socket.io').listen(9888);
//全局引用HAPI
global.HAPI = server;

module.exports = (dirname) => {
	let _package = require(`${dirname}/package.json`);
	let config = require(`${dirname}/config.json`);
	(async () => {
		//注册hapi插件
		await server.register(require('./lib/hapi/plugin')(config));
		var cat = new atweb({
			namespace: _package.name,
			name: _package.name,
			url: null,
			dirname: '',
			parent: null,
			_package,
			config
		});
		//加载root
		cat.use('/', dirname);
		//加载插件
		cat.use('/$', Path.resolve(__dirname + '/lib/plugin/*'));

		//socket.io prototype
        cat.addMethod('io', io);

		//转化成hapi路由
		global.ATWEB.rmap.forEach(ritem => {
			(function(route){
				route.config.handler = async function(request, h){
			    	//扩展request
					request = require('./lib/hapi/request')(request);
			    	//扩展内容输出
			    	h._resHtml = '';
					h = require('./lib/hapi/h')(request, h, route);
					//执行业务代码
					await route.handler(request,h);
					return h._resHtml;
			    };
		        route.config.tags = route.config.tags || [];
		        route.config.tags.push('api');
		        // route.config.pre = route.module.pre || [];
				server.route({
				    path: route.path,
				    method: route.method,
				    config: route.config
				});
				console.log(`${route.path} 载入成功`);
			})(ritem);
		});
		//启动hapi服务	
	    await server.start();
	    console.log(`应用启动成功`);
	    console.log(`访问地址: http://localhost:${config.dockerPort}`);
	})();
}