#更新npm依赖模块
cd /atweb
rm package.json
rm package-lock.json
cp /atweb/app/package.json /atweb/package.json
npm install --registry=https://registry.npm.taobao.org
cd /atweb
#开发
if [ "$1" = "dev" ];then
	#启动服务
	pm2 start web.js --name='app' --watch --ignore-watch='storage app/.git app/_data app/web app/package.json' -- /var/www/app
	cd /atweb/app/web
	fis3 release dev -wd /atweb/storage/assets
	pm2 logs app
fi
if [ "$1" = "online" ];then
	pm2 stop app
	pm2 start web.js --name='app' -- /var/www/app
	cd /atweb/app/web
	fis3 release online -wd /atweb/storage/assets
	pm2 logs app
fi